resource "aws_network_interface" "nicinstance1" {
  subnet_id   = aws_subnet.public_subnet_1.id
  private_ips = ["10.0.0.100"]

  tags = {
    Name = "Nic Instance 1"
  }
}

resource "aws_instance" "instance1" {
  ami           = data.aws_ami.image.id
  instance_type = "t2.micro"
 # key_name      = aws_key_pair.keypair.id
 # count = var.instance_number
  network_interface {
  #  network_interface_id = aws_instance.instance1[count.index].id
  network_interface_id = aws_network_interface.nicinstance1.id
    device_index         = 0
  }
 tags = {
     Name = "Terraform Instance"
       }

}

data "aws_ami" "image" {   # déclaration de la source de données de type aws_ami (ami aws)
  most_recent = true       # demande à avoir l'image la plus recente disponible
  owners      = ["amazon"] # lorsque le proriétaire de l'image s'appele amazon
  filter {                 # on ajoute un filtre  
    name   = "name"        # on veut filtrer l'image lorsque le nom à comme par amzn2-ami-hvm- , * pour n'importe quoi , et se termine par -x86_64-gp2
    values = ["amzn2-ami-hvm-*"]
  }
}

