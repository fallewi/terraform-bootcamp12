

resource "aws_nat_gateway" "natgw1" {
  subnet_id     = aws_subnet.private_subnet_1.id
  allocation_id = aws_eip.eip1.id
  tags = {
    Name = "Nat Gateway 1"
  }

  depends_on = [aws_internet_gateway.igw1]
}


resource "aws_nat_gateway" "natgw2" {
  subnet_id     = aws_subnet.private_subnet_2.id
  allocation_id = aws_eip.eip2.id
  tags = {
    Name = "Nat Gateway 2"
  }

  depends_on = [aws_internet_gateway.igw1]
}

